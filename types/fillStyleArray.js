class FillStyleArray extends Array {
  push(a) {
    const SWF = require('../index.js')
    if (!(a instanceof SWF.FillStyle))
      throw new Error('A FillStyleArray can only contains FillStyle objects')
    super.push(a)
  }
}

FillStyleArray.read = function(buffer, options = {}) {
  const SWF = require('../index.js')
  let fsa = new FillStyleArray()
  let length = buffer.readUInt8()

  if (length == 0xFF
      && (options.shapeVersion == 2
          || options.shapeVersion == 3
          || options.shapeVersion == 4)) {
    length = buffer.readUInt16()
  }
  for (let i = 0; i < length; ++i) {
    fsa.push(SWF.FillStyle.read(buffer, options))
  }
  return fsa
}

FillStyleArray.prototype.write = function(buffer, options) {
  let length = this.length

  if (length > 0xFE
      && (options.shapeVersion == 2
          || options.shapeVersion == 3
          || options.shapeVersion == 4)) {
    buffer.writeUInt8(0xFF)
    buffer.writeUInt16(length)
  } else {
    length = Math.min(length, 0xFF)
    buffer.writeUInt8(length)
  }
  this.slice(0, length).forEach(fs => fs.write(buffer, options))
}

FillStyleArray.prototype.getLength = function(options = {}) {
  let length = 1

  if (this.length > 0xFE
      && (options.shapeVersion == 2
          || options.shapeVersion == 3
          || options.shapeVersion == 4)) {
    length += 2
  }
  length += this.reduce((acc, fs) => acc + fs.getLength(options), 0)
  return length
}

module.exports = FillStyleArray
