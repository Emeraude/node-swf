function FocalGradient(spreadMode = 0, interpolationMode = 0, focalPoint = 0) {
  this.spreadMode = spreadMode
  this.interpolationMode = interpolationMode
  this.records = []
  this.focalPoint = focalPoint
}

FocalGradient.prototype.write = function(buffer, options = {}) {
  if (options.shapeVersion !== 4
      || this.spreadMode === 3
      || this.interpolationMode === 3
      || this.length > 15) {
    console.warn('Warning: Unexpected values found while writing a FOCALGRADIENT structure')
  }

  buffer.writeUBits(this.spreadMode, 2)
  buffer.writeUBits(this.interpolationMode, 2)
  buffer.writeUBits(Math.min(this.records.length, 15), 4)
  this.records.slice(0, 15).forEach(v => v.write(buffer, options))
  buffer.writeFixed8(this.focalPoint)
}

FocalGradient.read = function(buffer, options = {}) {
  const SWF = require('../index.js')
  const gradient = new FocalGradient(buffer.readUBits(2), buffer.readUBits(2))
  const length = buffer.readUBits(4)

  if (options.shapeVersion !== 4
      || gradient.spreadMode === 3
      || gradient.interpolationMode === 3) {
    console.warn('Warning: Got unexpected values while reading a FOCALGRADIENT structure')
  }
  for (let i = 0; i < length; ++i)
    gradient.records.push(new SWF.GradRecord.read(buffer, options));
  gradient.focalPoint = buffer.readFixed8()
  return gradient
}

FocalGradient.prototype.getLength = function(options = {}) {
  return 3 + this.records.reduce((acc, record) => acc + record.getLength(options), 0)
}

module.exports = FocalGradient
