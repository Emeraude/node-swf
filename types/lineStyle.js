function LineStyle(width = 0, color = null) {
  const SWF = require('../index.js')
  this.width = width
  this.color = color || new SWF.Color()
}

LineStyle.prototype.write = function(buffer, options = {}) {
  switch (options.shapeVersion) {
  case 1:
  case 2:
    buffer.writeUInt16(this.width)
    this.color.writeRGB(buffer)
    break

  case 3:
    buffer.writeUInt16(this.width)
    this.color.writeRGBA(buffer)
    break

  default:
    throw new Error('Invalid shape version `' + options.shapeVersion + '`')
  }
}

LineStyle.read = function(buffer, options = {}) {
  const SWF = require('../index.js')
  switch (options.shapeVersion) {
  case 1:
  case 2:
    return new LineStyle(buffer.readUInt16(),
                         SWF.Color.readRGB(buffer))

  case 3:
    return new LineStyle(buffer.readUInt16(),
                         SWF.Color.readRGBA(buffer))

  default:
    throw new Error('Invalid shape version `' + options.shapeVersion + '`')
  }
}

LineStyle.prototype.getLength = function(options = {}) {
  switch (options.shapeVersion) {
  case 1:
  case 2:
    return 5

  case 3:
    return 6

  default:
    throw new Error('Invalid shape version `' + options.shapeVersion + '`')
  }
}

module.exports = LineStyle
