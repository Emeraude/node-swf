const SWFBuffer = require('../swf-buffer.js')

function ColorTransformWithAlpha() {}

ColorTransformWithAlpha.prototype.write = function(buffer, options = {}) {
  let hasMultTerms = false
  let hasAddTerms = false
  let values = []

  if (this.redMult !== undefined && this.greenMult !== undefined
      && this.blueMult !== undefined && this.alphaMult !== undefined) {
    hasMultTerms = true
    values = values.concat([this.redMult, this.greenMult, this.blueMult, this.alphaMult])
  }
  if (this.redAdd !== undefined && this.greenAdd !== undefined
      && this.blueAdd !== undefined && this.alphaAdd !== undefined) {
    hasAddTerms = true
    values = values.concat([this.redAdd, this.greenAdd, this.blueAdd, this.alphaAdd])
  }

  const nbits = SWFBuffer.getNeededBits(values)
  buffer.writeUBits(hasAddTerms ? 1 : 0, 1)
  buffer.writeUBits(hasMultTerms ? 1 : 0, 1)
  buffer.writeUBits(nbits, 4)
  if (hasMultTerms) {
    buffer.writeSBits(this.redMult, nbits)
    buffer.writeSBits(this.greenMult, nbits)
    buffer.writeSBits(this.blueMult, nbits)
    buffer.writeSBits(this.alphaMult, nbits)
  }
  if (hasAddTerms) {
    buffer.writeSBits(this.redAdd, nbits)
    buffer.writeSBits(this.greenAdd, nbits)
    buffer.writeSBits(this.blueAdd, nbits)
    buffer.writeSBits(this.alphaAdd, nbits)
  }
  buffer.alignBits()
}

ColorTransformWithAlpha.read = function(buffer) {
  let cxform = new ColorTransformWithAlpha()

  let hasAddTerms = Boolean(buffer.readUBits(1))
  let hasMultTerms = Boolean(buffer.readUBits(1))
  let nbits = buffer.readUBits(4)

  if (hasMultTerms) {
    cxform.redMult = buffer.readSBits(nbits)
    cxform.greenMult = buffer.readSBits(nbits)
    cxform.blueMult = buffer.readSBits(nbits)
    cxform.alphaMult = buffer.readSBits(nbits)
  }
  if (hasAddTerms) {
    cxform.redAdd = buffer.readSBits(nbits)
    cxform.greenAdd = buffer.readSBits(nbits)
    cxform.blueAdd = buffer.readSBits(nbits)
    cxform.alphaAdd = buffer.readSBits(nbits)
  }
  buffer.alignBits()

  return cxform
}

ColorTransformWithAlpha.prototype.getLength = function(options = {}) {
  let values = []

  if (this.redMult !== undefined && this.greenMult !== undefined
      && this.blueMult !== undefined && this.alphaMult !== undefined) {
    values = values.concat([this.redMult, this.greenMult, this.blueMult, this.alphaMult])
  }
  if (this.redAdd !== undefined && this.greenAdd !== undefined
      && this.blueAdd !== undefined && this.alphaAdd !== undefined) {
    values = values.concat([this.redAdd, this.greenAdd, this.blueAdd, this.alphaAdd])
  }

  return Math.ceil((SWFBuffer.getNeededBits(values) * values.length + 6) / 8.0)
}

module.exports = ColorTransformWithAlpha
