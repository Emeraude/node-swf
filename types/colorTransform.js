const SWFBuffer = require('../swf-buffer.js')

function ColorTransform() {}

ColorTransform.prototype.write = function(buffer, options = {}) {
  let hasMultTerms = false
  let hasAddTerms = false
  let values = []

  if (this.redMult !== undefined
      && this.greenMult !== undefined
      && this.blueMult !== undefined) {
    hasMultTerms = true
    values = values.concat([this.redMult, this.greenMult, this.blueMult])
  }
  if (this.redAdd !== undefined
      && this.greenAdd !== undefined
      && this.blueAdd !== undefined) {
    hasAddTerms = true
    values = values.concat([this.redAdd, this.greenAdd, this.blueAdd])
  }

  const nbits = SWFBuffer.getNeededBits(values)
  buffer.writeUBits(hasAddTerms ? 1 : 0, 1)
  buffer.writeUBits(hasMultTerms ? 1 : 0, 1)
  buffer.writeUBits(nbits, 4)
  if (hasMultTerms) {
    buffer.writeSBits(this.redMult, nbits)
    buffer.writeSBits(this.greenMult, nbits)
    buffer.writeSBits(this.blueMult, nbits)
  }
  if (hasAddTerms) {
    buffer.writeSBits(this.redAdd, nbits)
    buffer.writeSBits(this.greenAdd, nbits)
    buffer.writeSBits(this.blueAdd, nbits)
  }
  buffer.alignBits()
}

ColorTransform.read = function(buffer) {
  let cxform = new ColorTransform()

  let hasAddTerms = Boolean(buffer.readUBits(1))
  let hasMultTerms = Boolean(buffer.readUBits(1))
  let nbits = buffer.readUBits(4)

  if (hasMultTerms) {
    cxform.redMult = buffer.readSBits(nbits)
    cxform.greenMult = buffer.readSBits(nbits)
    cxform.blueMult = buffer.readSBits(nbits)
  }
  if (hasAddTerms) {
    cxform.redAdd = buffer.readSBits(nbits)
    cxform.greenAdd = buffer.readSBits(nbits)
    cxform.blueAdd = buffer.readSBits(nbits)
  }
  buffer.alignBits()

  return cxform
}

ColorTransform.prototype.getLength = function(options = {}) {
  let values = []

  if (this.redMult !== undefined
      && this.greenMult !== undefined
      && this.blueMult !== undefined) {
    values = values.concat([this.redMult, this.greenMult, this.blueMult])
  }
  if (this.redAdd !== undefined
      && this.greenAdd !== undefined
      && this.blueAdd !== undefined) {
    values = values.concat([this.redAdd, this.greenAdd, this.blueAdd])
  }

  return Math.ceil((SWFBuffer.getNeededBits(values) * values.length + 6) / 8.0)
}

module.exports = ColorTransform
