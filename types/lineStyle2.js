function LineStyle2(width = 0) {
  this.width = width
  this.startCapStyle = 0
  this.joinStyle = 0
  this.noHScale = false
  this.noVScale = false
  this.pixelHinting = false
  this.noClose = false
  this.endCapStyle = 0
}

LineStyle2.prototype.write = function(buffer, options = {}) {
  const SWF = require('../index.js')
  if (this.startCapStyle === 3
      || this.joinStyle === 3
      || this.endCapStyle === 3)
    console.warn('Warning: got invalid value while writing LINESTYLE2 type')
  if ((this.color instanceof SWF.Color)
      ^ (this.fillStyle instanceof SWF.FillStyle) == 0)
    throw new Error('LineStyle2 should contain either a valid `color` or a valid `fillStyle` field')
  if (this.joinStyle === 2
      && this.miterLimitFactor === undefined)
    console.warn('Warning: miterLimitFactor not set while writing a LINESTYLE2 with a joinStyle of 2')

  let hasFill = this.fillStyle !== undefined
  buffer.writeUInt16(this.width)
  buffer.writeUBits(this.startCapStyle, 2)
  buffer.writeUBits(this.joinStyle, 2)
  buffer.writeUBits(this.color !== undefined ? 1 : 0, 1)
  buffer.writeUBits(this.noHScale == true ? 1 : 0, 1)
  buffer.writeUBits(this.noVScale == true ? 1 : 0, 1)
  buffer.writeUBits(this.pixelHinting == true ? 1 : 0, 1)
  buffer.writeUBits(0, 5)
  buffer.writeUBits(this.noClose == true ? 1 : 0, 1)
  buffer.writeUBits(this.endCapStyle, 2)

  if (this.joinStyle === 2)
    buffer.writeFixed8(this.miterLimitFactor || 0.0)
  if (hasFill === true)
    this.fillStyle.write(buffer, options)
  else
    this.color.writeRGBA(buffer, options)
}

LineStyle2.read = function(buffer, options = {}) {
  const SWF = require('../index.js')
  let ls = new LineStyle2()

  ls.width = buffer.readUInt16()
  ls.startCapStyle = buffer.readUBits(2)
  ls.joinStyle = buffer.readUBits(2)
  let hasFill = Boolean(buffer.readUBits(1))
  ls.noHScale = Boolean(buffer.readUBits(1))
  ls.noVScale = Boolean(buffer.readUBits(1))
  ls.pixelHinting = Boolean(buffer.readUBits(1))
  buffer.readUBits(5)
  ls.noClose = Boolean(buffer.readUBits(1))
  ls.endCapStyle = buffer.readUBits(2)

  if (ls.startCapStyle === 3
      || ls.joinStyle === 3
      || ls.endCapStyle === 3)
    console.warn('Warning: got invalid value while reading LINESTYLE2 type')

  if (ls.joinStyle === 2)
    ls.miterLimitFactor = buffer.readFixed8()
  if (hasFill === true) {
    this.fillStyle = SWF.FillStyle.read(buffer, options)
  } else {
    this.color = SWF.Color.readRGBA(buffer)
  }
  return ls
}

LineStyle2.prototype.getLength = function(options = {}) {
  return 2 // width
    + 2 // flags
    + (this.joinStyle === 2 ? 2 : 0)
    + (this.color !== undefined ? 4 : this.fillStyle.getLength(options)) // color or fillStyle
}

module.exports = LineStyle2
