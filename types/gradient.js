function Gradient(spreadMode = 0, interpolationMode = 0) {
  this.spreadMode = spreadMode
  this.interpolationMode = interpolationMode
  this.records = []
}

Gradient.prototype.write = function(buffer, options = {}) {
  if ((options.shapeVersion !== 4
       && (this.spreadMode !== 0
           || this.interpolationMode !== 0
           || this.length > 8))
      || this.spreadMode === 3
      || this.interpolationMode === 3
      || this.length > 15) {
    console.warn('Warning: Unexpected values found while writing a GRADIENT structure')
  }

  buffer.writeUBits(this.spreadMode, 2)
  buffer.writeUBits(this.interpolationMode, 2)
  buffer.writeUBits(Math.min(this.records.length, 15), 4)
  this.records.slice(0, 15).forEach(v => v.write(buffer, options))
}

Gradient.read = function(buffer, options = {}) {
  const SWF = require('../index.js')
  const gradient = new Gradient(buffer.readUBits(2), buffer.readUBits(2))
  const length = buffer.readUBits(4)

  if ((options.shapeVersion !== 4
       && (gradient.spreadMode !== 0
           || gradient.interpolationMode !== 0
           || length > 8))
      || gradient.spreadMode === 3
      || gradient.interpolationMode === 3) {
    console.warn('Warning: Got unexpected values while reading a GRADIENT structure')
  }
  for (let i = 0; i < length; ++i)
    gradient.records.push(new SWF.GradRecord.read(buffer, options));
  return gradient
}

Gradient.prototype.getLength = function(options = {}) {
  return 1 + this.records.reduce((acc, record) => acc + record.getLength(options), 0)
}

module.exports = Gradient
