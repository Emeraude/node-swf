const SWFBuffer = require('../swf-buffer.js')

function Matrix() {
  this.translateX = 0
  this.translateY = 0
}

Matrix.prototype.write = function(buffer, options = {}) {
  if (this.scaleX !== undefined && this.scaleY !== undefined) {
    let bits = SWFBuffer.getNeededBits([this.scaleX, this.scaleY])
    if (options.keepAnomalies == true && this.__oddNScaleBits !== undefined)
      bits = Math.max(bits, this.__oddNScaleBits)
    buffer.writeUBits(1, 1)
    buffer.writeUBits(bits, 5)
    buffer.writeFBits(this.scaleX, bits)
    buffer.writeFBits(this.scaleY, bits)
  } else {
    buffer.writeUBits(0, 1)
  }

  if (this.rotateSkew0 !== undefined && this.rotateSkew1 !== undefined) {
    let bits = SWFBuffer.getNeededBits([this.rotateSkew0, this.rotateSkew1])
    if (options.keepAnomalies == true && this.__oddNRotateBits !== undefined)
      bits = Math.max(bits, this.__oddNRotateBits)
    buffer.writeUBits(1, 1)
    buffer.writeUBits(bits, 5)
    buffer.writeFBits(this.rotateSkew0, bits)
    buffer.writeFBits(this.rotateSkew1, bits)
  } else {
    buffer.writeUBits(0, 1)
  }

  let bits = SWFBuffer.getNeededBits([this.translateX, this.translateY])
  if (options.keepAnomalies == true && this.__oddNTranslateBits !== undefined)
    bits = Math.max(bits, this.__oddNTranslateBits)
  buffer.writeUBits(bits, 5)
  buffer.writeFBits(this.translateX, bits)
  buffer.writeFBits(this.translateY, bits)
  buffer.alignBits()
}

Matrix.read = function(buffer) {
  let matrix = new Matrix()

  let hasScale = Boolean(buffer.readUBits(1))
  if (hasScale == true) {
    let bits = buffer.readUBits(5)
    matrix.scaleX = buffer.readFBits(bits)
    matrix.scaleY = buffer.readFBits(bits)
    if (bits > SWFBuffer.getNeededBits([matrix.scaleX, matrix.scaleY])) {
      console.warn('Warning: This matrix is oddly encoded')
      matrix.__oddNScaleBits = bits
    }
  }

  let hasRotate = Boolean(buffer.readUBits(1))
  if (hasRotate == true) {
    let bits = buffer.readUBits(5)
    matrix.rotateSkew0 = buffer.readFBits(bits)
    matrix.rotateSkew1 = buffer.readFBits(bits)
    if (bits > SWFBuffer.getNeededBits([matrix.rotateSkew0, matrix.rotateSkew1])) {
      console.warn('Warning: This matrix is oddly encoded')
      matrix.__oddNRotateBits = bits
    }
  }

  let bits = buffer.readUBits(5)
  matrix.translateX = buffer.readSBits(bits)
  matrix.translateY = buffer.readSBits(bits)
  if (bits > SWFBuffer.getNeededBits([matrix.translateX, matrix.translateY])) {
    console.warn('Warning: This matrix is oddly encoded')
    matrix.__oddNTranslateBits = bits
  }
  buffer.alignBits()
  return matrix
}

Matrix.prototype.getLength = function(options = {}) {
  let bits = 0
  if (this.scaleX !== undefined && this.scaleY !== undefined) {
    let scaleBits = SWFBuffer.getNeededBits([this.scaleX, this.scaleY])
    if (options.keepAnomalies == true && this.__oddNScaleBits !== undefined)
      scaleBits = Math.max(scaleBits, this.__oddNScaleBits)
    bits += scaleBits * 2 + 5
  }

  if (this.rotateSkew0 !== undefined && this.rotateSkew1 !== undefined) {
    let rotateBits = SWFBuffer.getNeededBits([this.rotateSkew0, this.rotateSkew1])
    if (options.keepAnomalies == true && this.__oddNRotateBits !== undefined)
      rotateBits = Math.max(rotateBits, this.__oddNRotateBits)
    bits += rotateBits * 2 + 5
  }

  let translateBits = SWFBuffer.getNeededBits([this.translateX, this.translateY])
  if (options.keepAnomalies == true && this.__oddNTranslateBits !== undefined)
    translateBits = Math.max(translateBits, this.__oddNTranslateBits)
  bits += translateBits * 2 + 5

  return Math.ceil((bits + 2) / 8.0)
}

module.exports = Matrix
