function GradRecord(ratio = 0, color = null) {
  const SWF = require('../index.js')
  this.ratio = ratio
  this.color = color || new SWF.Color()
}

GradRecord.prototype.write = function(buffer, options = {}) {
  switch (options.shapeVersion) {
  case 1:
  case 2:
    buffer.writeUInt8(this.ratio)
    this.color.writeRGB(buffer)
    break

  case 3:
  case 4:
    buffer.writeUInt8(this.ratio)
    this.color.writeRGBA(buffer)
    break

  default:
    throw new Error('Invalid shape version `' + options.shapeVersion + '`')
  }
}

GradRecord.read = function(buffer, options = {}) {
  const SWF = require('../index.js')
  switch (options.shapeVersion) {
  case 1:
  case 2:
    return new GradRecord(buffer.readUInt8(),
                          SWF.Color.readRGB(buffer))

  case 3:
  case 4:
    return new GradRecord(buffer.readUInt8(),
                          SWF.Color.readRGBA(buffer))

  default:
    throw new Error('Invalid shape version `' + options.shapeVersion + '`')
  }
}

GradRecord.prototype.getLength = function(options = {}) {
  switch (options.shapeVersion) {
  case 1:
  case 2:
    return 4

  case 3:
  case 4:
    return 5

  default:
    throw new Error('Invalid shape version `' + options.shapeVersion + '`')
  }
}

module.exports = GradRecord
