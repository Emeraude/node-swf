const SWFBuffer = require('../swf-buffer.js')

function Rect() {
  this.Xmin = 0
  this.Xmax = 0
  this.Ymin = 0
  this.Ymax = 0
}

Rect.prototype.write = function(buffer, options = {}) {
  const bits = SWFBuffer.getNeededBits([this.Xmin, this.Xmax, this.Ymin, this.Ymax])

  buffer.writeUBits(bits, 5)
  buffer.writeSBits(this.Xmin, bits)
  buffer.writeSBits(this.Xmax, bits)
  buffer.writeSBits(this.Ymin, bits)
  buffer.writeSBits(this.Ymax, bits)
  buffer.alignBits()
}

Rect.read = function(buffer) {
  let rect = new Rect()
  let bits = buffer.readUBits(5)

  rect.Xmin = buffer.readSBits(bits);
  rect.Xmax = buffer.readSBits(bits);
  rect.Ymin = buffer.readSBits(bits);
  rect.Ymax = buffer.readSBits(bits);
  buffer.alignBits()
  return rect
}

Rect.prototype.getLength = function(options = {}) {
  const bits = SWFBuffer.getNeededBits([this.Xmin, this.Xmax, this.Ymin, this.Ymax])

  return Math.ceil((bits * 4 + 5) / 8.0)
}

module.exports = Rect
