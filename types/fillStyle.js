const availableFillTypes = [
  0x00, 0x10, 0x12, 0x13, 0x40, 0x41, 0x42, 0x43
]

function FillStyle(type = 0) {
  this.type = type
  if (availableFillTypes.indexOf(this.type) == -1)
    throw new Error('Invalid fill style type provided')
}

// TODO: add warning if invalid SWF file version
FillStyle.prototype.write = function(buffer, options = {}) {
  buffer.writeUInt8(this.type)
  switch (this.type) {
  case 0x00:
    switch (options.shapeVersion) {
    case 1:
    case 2:
      this.color.writeRGB(buffer)
      break

    case 4:
      console.warn('Using fill style type 0x00 with a Shape4.')
    case 3:
      this.color.writeRGBA(buffer)
      break

    default:
      throw new Error('Invalid shape version `' + options.shapeVersion + '`')
    }
    break

  case 0x10:
  case 0x12:
  case 0x13: // Only with Shape4 and SWF >= 8
    this.gradientMatrix.write(buffer, options)
    this.gradient.write(buffer, options)
    break

  case 0x40:
  case 0x41:
  case 0x42:
  case 0x43:
    buffer.writeUInt16(this.bitmapId)
    this.bitmapMatrix.write(buffer)
    break

  default:
    throw new Error('Invalid fill style type provided')
  }
}

FillStyle.read = function(buffer, options = {}) {
  const SWF = require('../index.js')

  let fs = new FillStyle(buffer.readUInt8())
  switch (fs.type) {
  case 0x00:
    switch (options.shapeVersion) {
    case 1:
    case 2:
      fs.color = new SWF.Color.readRGB(buffer)
      break

    case 4:
      console.warn('Using fill style type 0x00 with a Shape4.')
    case 3:
      fs.color = new SWF.Color.readRGBA(buffer)
      break

    default:
      throw new Error('Invalid shape version `' + options.shapeVersion + '`')
    }
    break

  case 0x10:
  case 0x12:
    fs.gradientMatrix = new SWF.Matrix.read(buffer, options)
    fs.gradient = new SWF.Gradient.read(buffer, options)
    break

  case 0x13: // Only with Shape4 and SWF >= 8
    fs.gradientMatrix = new SWF.Matrix.read(buffer, options)
    fs.gradient = new SWF.FocalGradient.read(buffer, options)
    break

  case 0x40:
  case 0x41:
  case 0x42:
  case 0x43:
    fs.bitmapId = buffer.readUInt16()
    fs.bitmapMatrix = new SWF.Matrix.read(buffer)
    break

  default:
    throw new Error('Invalid fill style type provided')
  }
  return fs
}

FillStyle.prototype.getLength = function(options = {}) {
  let length = 1

  switch (this.type) {
  case 0x00:
    if (options.shapeVersion == 1 || options.shapeVersion == 2)
      length += 3
    else if (options.shapeVersion == 3 || options.shapeVersion == 4)
      length += 4
    else
      throw new Error('Invalid shape version `' + options.shapeVersion + '`')
    break

  case 0x10:
  case 0x12:
  case 0x13:
    length += this.gradientMatrix.getLength()
      + this.gradient.getLength(options)
    break

  case 0x40:
  case 0x41:
  case 0x42:
  case 0x43:
    length += 2 + this.bitmapMatrix.getLength()
    break

  default:
    throw new Error('Invalid fill style type provided')
  }
  return length
}

module.exports = FillStyle
