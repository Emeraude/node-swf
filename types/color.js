function Color(red = 0, green = 0, blue = 0, alpha) {
  this.red = red
  this.green = green
  this.blue = blue
  this.alpha = alpha
}

Color.prototype.writeRGB = function(buffer, options = {}) {
  buffer.writeUInt8(this.red)
  buffer.writeUInt8(this.green)
  buffer.writeUInt8(this.blue)
}

Color.prototype.writeRGBA = function(buffer, options = {}) {
  buffer.writeUInt8(this.red)
  buffer.writeUInt8(this.green)
  buffer.writeUInt8(this.blue)
  buffer.writeUInt8(this.alpha)
}

Color.prototype.writeARGB = function(buffer, options = {}) {
  buffer.writeUInt8(this.alpha)
  buffer.writeUInt8(this.red)
  buffer.writeUInt8(this.green)
  buffer.writeUInt8(this.blue)
}

Color.readRGB = function(buffer) {
  return new Color(buffer.readUInt8(),
                   buffer.readUInt8(),
                   buffer.readUInt8())
}

Color.readRGBA = function(buffer) {
  return new Color(buffer.readUInt8(),
                   buffer.readUInt8(),
                   buffer.readUInt8(),
                   buffer.readUInt8())
}

Color.readARGB = function(buffer) {
  const alpha = buffer.readUInt8()
  const red = buffer.readUInt8()
  const green = buffer.readUInt8()
  const blue = buffer.readUInt8()

  return new Color(red, green, blue, alpha)
}

Color.prototype.getLength = function(options = {}) {
  return this.alpha !== undefined ? 4 : 3
}

module.exports = Color
