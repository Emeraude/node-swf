function SWFBuffer(buffer) {
  this.buffer = buffer
  this.pos = 0
  this.bitPos = 1
}

SWFBuffer.prototype.readUInt8 = function() {
  return this.buffer.readUInt8(this.pos++)
}

SWFBuffer.prototype.readUInt16 = function() {
  this.pos += 2
  return this.buffer.readUInt16LE(this.pos - 2)
}

SWFBuffer.prototype.readUInt32 = function() {
  this.pos += 4
  return this.buffer.readUInt32LE(this.pos - 4)
}

SWFBuffer.prototype.readSInt8 = function() {
  return this.buffer.readInt8(this.pos++)
}

SWFBuffer.prototype.readSInt16 = function() {
  this.pos += 2
  return this.buffer.readInt16LE(this.pos - 2)
}

SWFBuffer.prototype.readSInt32 = function() {
  this.pos += 4
  return this.buffer.readInt32LE(this.pos - 4)
}

SWFBuffer.prototype.readUInt8Array = function(n) {
  const array = []

  while (n--)
    array.push(this.readUInt8())
  return array
}

SWFBuffer.prototype.readUInt16Array = function(n) {
  const array = []

  while (n--)
    array.push(this.readUInt16())
  return array
}

SWFBuffer.prototype.readUInt24Array = function(n) {
  const array = []

  while (n--) {
    array.push(this.readUInt8()
               | (this.readUInt8() << 8)
               | (this.readUInt8() << 16))
  }
  return array
}

SWFBuffer.prototype.readUInt32Array = function(n) {
  const array = []

  while (n--)
    array.push(this.readUInt32())
  return array
}

SWFBuffer.prototype.readUInt64Array = function(n) {
  const array = []

  while (n--) {
    array.push(BigInt(this.readUInt8()
                      + (this.readUInt8() << 8)
                      + (this.readUInt8() << 16)
                      + (this.readUInt8() << 24)
                      + (this.readUInt8() * Math.pow(2, 32))
                      + (this.readUInt8() * Math.pow(2, 40)))
               + (BigInt(this.readUInt8()) * BigInt(Math.pow(2, 48)))
               + (BigInt(this.readUInt8()) * BigInt(Math.pow(2, 56))))
  }
  return array
}

SWFBuffer.prototype.readSInt8Array = function(n) {
  const array = []

  while (n--)
    array.push(this.readSInt8())
  return array
}

SWFBuffer.prototype.readSInt16Array = function(n) {
  const array = []

  while (n--)
    array.push(this.readSInt16())
  return array
}

SWFBuffer.prototype.readBits = function(bits, signed) {
  let n = 0
  let r = 0
  if (bits && signed) {
    ++n
    if (((this.buffer[this.pos] >> (8 - this.bitPos++)) & 1))
      r = -1
    if (this.bitPos > 8)
      this.alignBits()
  }

  while (n++ < bits) {
    r = (r << 1) + ((this.buffer[this.pos] >> (8 - this.bitPos++)) & 1)
    if (this.bitPos > 8)
      this.alignBits()
  }
  return r
}

SWFBuffer.prototype.readUBits = function(bits) {
  return this.readBits(bits, false)
}

SWFBuffer.prototype.readSBits = function(bits) {
  return this.readBits(bits, true)
}

SWFBuffer.prototype.readFBits = function(bits) {
  return this.readBits(bits, true)
}

SWFBuffer.prototype.alignBits = function() {
  if (this.bitPos != 1) {
    this.pos++
    this.bitPos = 1
  }
}

SWFBuffer.prototype.readFixed = function() {
  return this.readSInt32() / 65536
}

SWFBuffer.prototype.readFixed8 = function() {
  return this.readSInt16() / 256
}

SWFBuffer.prototype.readString = function() {
  let str = ''
  let lastRead = 0

  while ((lastRead = this.readUInt8()) != 0) {
    str += String.fromCharCode(lastRead)
  }
  return str
}

SWFBuffer.prototype.readBytes = function(size) {
  let buf = this.buffer.slice(this.pos, this.pos + size)

  this.pos += size
  return buf
}

SWFBuffer.prototype.writeUInt8 = function(v) {
  return this.buffer.writeUInt8(v, this.pos++)
}

SWFBuffer.prototype.writeUInt16 = function(v) {
  this.pos += 2
  return this.buffer.writeUInt16LE(v, this.pos - 2)
}

SWFBuffer.prototype.writeUInt32 = function(v) {
  this.pos += 4
  return this.buffer.writeUInt32LE(v, this.pos - 4)
}

SWFBuffer.prototype.writeSInt8 = function(v) {
  return this.buffer.writeInt8(v, this.pos++)
}

SWFBuffer.prototype.writeSInt16 = function(v) {
  this.pos += 2
  return this.buffer.writeInt16LE(v, this.pos - 2)
}

SWFBuffer.prototype.writeSInt32 = function(v) {
  this.pos += 4
  return this.buffer.writeInt32LE(v, this.pos - 4)
}

SWFBuffer.prototype.writeUInt8Array = function(array) {
  for (let i = 0; i < array.length; ++i)
    this.writeUInt8(array[i])
}

SWFBuffer.prototype.writeUInt16Array = function(array) {
  for (let i = 0; i < array.length; ++i)
    this.writeUInt16(array[i])
}

SWFBuffer.prototype.writeUInt24Array = function(array) {
  for (let i = 0; i < array.length; ++i) {
    this.writeUInt8(array[i] & 0xFF)
    this.writeUInt8((array[i] >> 8) & 0xFF)
    this.writeUInt8((array[i] >> 16) & 0xFF)
  }
}

SWFBuffer.prototype.writeUInt32Array = function(array) {
  for (let i = 0; i < array.length; ++i)
    this.writeUInt32(array[i])
}

SWFBuffer.prototype.writeUInt64Array = function(array) {
  for (let i = 0; i < array.length; ++i) {
    const nb = BigInt(array[i])
    this.writeUInt8(Number(nb & 0xFFn))
    this.writeUInt8(Number((nb >> 8n) & 0xFFn))
    this.writeUInt8(Number((nb >> 16n) & 0xFFn))
    this.writeUInt8(Number((nb >> 24n) & 0xFFn))
    this.writeUInt8(Number(nb / BigInt(Math.pow(2, 32)) & 0xFFn))
    this.writeUInt8(Number(nb / BigInt(Math.pow(2, 40)) & 0xFFn))
    this.writeUInt8(Number(nb / BigInt(Math.pow(2, 48)) & 0xFFn))
    this.writeUInt8(Number(nb / BigInt(Math.pow(2, 56)) & 0xFFn))
  }
}

SWFBuffer.prototype.writeSInt8Array = function(array) {
  for (let i = 0; i < array.length; ++i)
    this.writeSInt8(array[i])
}

SWFBuffer.prototype.writeSInt16Array = function(array) {
  for (let i = 0; i < array.length; ++i)
    this.writeSInt16(array[i])
}

SWFBuffer.prototype.writeBits = function(v, bits) {
  let n = 0

  while (++n <= bits) {
    this.buffer[this.pos] |= (((v >> (bits - n) & 1)) << (8 - this.bitPos))
    this.bitPos += 1
    if (this.bitPos > 8)
      this.alignBits()
  }
}

SWFBuffer.prototype.writeUBits = function(v, bits) {
  return this.writeBits(v, bits)
}

SWFBuffer.prototype.writeSBits = function(v, bits) {
  return this.writeBits(v, bits)
}

SWFBuffer.prototype.writeFBits = function(v, bits) {
  return this.writeBits(v, bits)
}

SWFBuffer.prototype.writeFixed = function(v) {
  return this.writeSInt32(parseInt(v * 65536))
}

SWFBuffer.prototype.writeFixed8 = function(v) {
  return this.writeSInt16(parseInt(v * 256))
}

SWFBuffer.prototype.writeString = function(str) {
  for (let i in str) {
    this.writeUInt8(str.charCodeAt(i))
  }
  this.writeUInt8(0)
}

SWFBuffer.prototype.writeBytes = function(bytes) {
  this.pos += bytes.copy(this.buffer, this.pos)
}

SWFBuffer.getNeededBits = function(values) {
  if (values.length < 1)
    return 0

  const max = values.map(v => v < 0 ? -v + 1 : v)
        .reduce((max, v) => max > v ? max : v)
  return Math.ceil(max ? Math.log2(max + 1) : -1) + 1
}

module.exports = SWFBuffer
