#!/usr/bin/env node

const path = require('path')
const SWF = require('../index.js')
const getTagClass = require('../swf-tags.js').getTagClass

const FILEPATH = process.env.SWF_FILE || path.join(__dirname, './hammerfest/loader.swf')

/**
 * This script opens a SWF file, and display all different tags types found in it
 **/

SWF.readFile(FILEPATH, function(err, swf) {
  if (err)
    throw err

  const expr = t => [t.type].concat(t.tags ? t.tags.flatMap(expr) : [])
  console.log([...new Set(swf.tags.flatMap(expr))]
              .sort((a, b) => a - b)
              .map(id => id + ' (' + getTagClass(id).name + ')'))
})
