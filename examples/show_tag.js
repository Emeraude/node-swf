#!/usr/bin/env node

const path = require('path')
const SWF = require('../index.js')

const FILEPATH = process.env.SWF_FILE || path.join(__dirname, './hammerfest/loader.swf')

/**
 * This script displays all the tags which has one of the tag_ids specified in parameter
 **/

if (process.argv.length < 3) {
  console.error('Usage: ' + __filename + ' <tag_id>...')
  process.exit(1)
}

const argv = process.argv.slice(2)
      .map(a => parseInt(a))

SWF.readFile(FILEPATH, function(err, swf) {
  if (err)
    throw err

  swf.tags.filter(t => argv.indexOf(t.type) != -1)
    .forEach((t) => console.log(t))
})
