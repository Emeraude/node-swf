#!/usr/bin/env node

const path = require('path')
const SWF = require('../index.js')

const FILEPATH = process.env.SWF_FILE || path.join(__dirname, './green.swf')

/**
 * This script opens a SWF file, display its header and all its tags, then reencode it.
 * In most of the cases, the input and the output files should be identical
 **/

SWF.readFile(FILEPATH, function(err, swf) {
  if (err)
    throw err
  console.log(swf.header)
  console.log(swf.tags)
  swf.writeFile(FILEPATH + '.copied', {keepAnomalies: true}, function(err) {
    if (err)
      throw err
  })
})
