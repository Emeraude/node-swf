const SWFBuffer = require('./swf-buffer.js')

const Tag = {}
Tag.End = require('./tags/end.js')
Tag.ShowFrame = require('./tags/showFrame.js')
Tag.SetBackgroundColor = require('./tags/setBackgroundColor.js')
Tag.RemoveObject2 = require('./tags/removeObject2.js')
Tag.DefineSprite = require('./tags/defineSprite.js')
Tag.DefineEditText = require('./tags/defineEditText.js')
Tag.ExportAssets = require('./tags/exportAssets.js')

function AbstractTag(tag) {
  this.type = tag.type
  this.body = tag.body
}

AbstractTag.decode = function(tag) {
  return new AbstractTag(tag)
}

AbstractTag.prototype.encode = function(options = {}) {
  return new SWFBuffer(this.body)
}

AbstractTag.prototype.getLength = function() {
  return this.body.length
}

function getTagClass(id) {
  const array = {
    0: Tag.End,
    1: Tag.ShowFrame,
    9: Tag.SetBackgroundColor,
    28: Tag.RemoveObject2,
    // 37: Tag.DefineEditText,
    39: Tag.DefineSprite,
    56: Tag.ExportAssets
  }
  return array[id] || AbstractTag
}

module.exports.getTagClass = getTagClass
module.exports.Tag = Tag
