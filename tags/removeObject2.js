let SWFBuffer = require('../swf-buffer.js')

function RemoveObject2(tag, options = {}) {
  this.type = 28
  this.depth = 0

  if (options.swfVersion !== undefined && options.swfVersion < 3 ) {
    console.warn('Warning: the minimal SWF version for RemoveObject2 is SWF 3.')
  }
  if (tag) {
    let buffer = new SWFBuffer(tag.body)
    this.depth = buffer.readUInt16()
  }
}

RemoveObject2.decode = function(buf) {
  return new RemoveObject2({body: buf})
}

RemoveObject2.prototype.encode = function(options = {}) {
  let buffer = new SWFBuffer(Buffer.alloc(2), options)

  buffer.writeUInt16(this.depth)
  return buffer
}

RemoveObject2.prototype.getLength = function() {
  return 2
}

module.exports = RemoveObject2
