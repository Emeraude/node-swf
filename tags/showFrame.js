let SWFBuffer = require('../swf-buffer.js')

function ShowFrame(tag) {
  this.type = 1
}

ShowFrame.decode = function(buf) {
  return new ShowFrame({body: buf})
}

ShowFrame.prototype.encode = function(options = {}) {
  return new SWFBuffer(Buffer.alloc(0))
}

ShowFrame.prototype.getLength = function() {
  return 0
}

module.exports = ShowFrame
