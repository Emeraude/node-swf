let SWFBuffer = require('../swf-buffer.js')

function End(tag) {
  this.type = 0
}

End.decode = function(buf) {
  return new End({body: buf})
}

End.prototype.encode = function(options = {}) {
  return new SWFBuffer(Buffer.alloc(0))
}

End.prototype.getLength = function() {
  return 0
}

module.exports = End
