let SWFBuffer = require('../swf-buffer.js')

function DefineEditText(tag) {
  this.type = 37
  let buffer = new SWFBuffer(tag.body)
  this.characterID = buffer.readUInt16()
  this.bound = buffer.readRect()
  let hasText = Boolean(buffer.readUBits(1))
  this.wordWrap = Boolean(buffer.readUBits(1))
  this.multiline = Boolean(buffer.readUBits(1))
  this.password = Boolean(buffer.readUBits(1))
  this.readOnly = Boolean(buffer.readUBits(1))
  let hasTextColor = Boolean(buffer.readUBits(1))
  let hasMaxLength = Boolean(buffer.readUBits(1))
  let hasFont = Boolean(buffer.readUBits(1))
  let hasFontClass = Boolean(buffer.readUBits(1))
  if (hasFont == true && hasFontClass == true)
    console.warn('Warning: DefineEditText tags cannot have both HasFont and HasFontClass set to true')
  this.autoSize = Boolean(buffer.readUBits(1))
  let hasLayout = Boolean(buffer.readUBits(1))
  this.noSelect = Boolean(buffer.readUBits(1))
  this.border = Boolean(buffer.readUBits(1))
  this.wasStatic = Boolean(buffer.readUBits(1))
  this.HTML = Boolean(buffer.readUBits(1))
  this.useOutlines = Boolean(buffer.readUBits(1))

  if (hasFont == true)
    this.fontID = buffer.readUInt16()
  if (hasFontClass == true)
    this.fontClass = buffer.readString()
  if (hasFont == true)
    this.fontHeight = buffer.readUInt16()
  if (hasTextColor == true)
    this.textColor = buffer.readRGBA()
  if (hasMaxLength == true)
    this.maxLength = buffer.readUInt16()
  if (hasLayout == true) {
    this.layout = {
      align: buffer.readUInt8(), // 0 = Left; 1 = Right; 2 = Center; 3 = Justify
      leftMargin: buffer.readUInt16(),
      rightMargin: buffer.readUInt16(),
      indent: buffer.readUInt16(),
      leading: buffer.readSInt16()
    }
  }
  this.variableName = buffer.readString()
  if (hasText == true)
    this.initialText = buffer.readString()
}

DefineEditText.decode = function(buf) {
  return new DefineEditText({body: buf})
}

DefineEditText.prototype.encode = function(options = {}) {
  let buffer = new SWFBuffer(Buffer.alloc(this.getLength()))

  buffer.writeUInt16(this.characterID)
  buffer.writeRect(this.bound)
  buffer.writeUBits(hasText ? 1 : 0, 1)
  buffer.writeUBits(this.wordWrap ? 1 : 0, 1)
  buffer.writeUBits(this.multiline ? 1 : 0, 1)
  buffer.writeUBits(this.password ? 1 : 0, 1)
  buffer.writeUBits(this.readOnly ? 1 : 0, 1)
  buffer.writeUBits(hasTextColor ? 1 : 0, 1)
  buffer.writeUBits(hasMaxLength ? 1 : 0, 1)
  buffer.writeUBits(hasFont ? 1 : 0, 1)
  buffer.writeUBits(hasFontClass ? 1 : 0, 1)
  buffer.writeUBits(this.autoSize ? 1 : 0, 1)
  buffer.writeUBits(hasLayout ? 1 : 0, 1)
  buffer.writeUBits(this.noSelect ? 1 : 0, 1)
  buffer.writeUBits(this.border ? 1 : 0, 1)
  buffer.writeUBits(this.wasStatic ? 1 : 0, 1)
  buffer.writeUBits(this.HTML ? 1 : 0, 1)
  buffer.writeUBits(this.useOutlines ? 1 : 0, 1)

  if (hasFont)
    buffer.writeUInt16(this.fontID)
  if (hasFontClass)
    buffer.writeString(this.fontClass)
  if (hasFont)
    buffer.writeUInt16(this.fontHeight)
  if (hasTextColor)
    buffer.writeRGBA(this.textColor)
  if (hasMaxLength)
    buffer.writeUInt16(this.maxLength)
  if (hasLayout) {
    buffer.writeUInt8(this.layout.align)
    buffer.writeUInt16(this.layout.leftMargin)
    buffer.writeUInt16(this.layout.rightMargin)
    buffer.writeUInt16(this.layout.indent)
    buffer.writeSInt16(this.layout.leading)
  }
  buffer.writeString(this.variableName)
  if (hasText)
    buffer.writeString(this.initialText)
  return buffer
}

DefineEditText.prototype.getLength = function() {
  let hasText = this.initialText !== undefined
  let hasFont = this.fontID !== undefined || this.fontHeight !== undefined
  let hasFontClass = this.fontClass !== undefined
  let hasTextColor = this.textColor !== undefined
  let hasMaxLength = this.maxLength !== undefined
  let hasLayout = this.layout !== undefined
  return 2 // characteID
      + SWFBuffer.getRectLength(this.bound)
      + 2 // 16 flag bits
      + (hasFont ? 4 : 0)
      + (hasFontClass ? this.fontClass.length + 1 : 0)
      + (hasTextColor ? 4 : 0)
      + (hasMaxLength ? 2 : 0)
      + (hasLayout ? 9 : 0)
      + this.variableName.length + 1
      + (hasText ? this.initialText.length + 1 : 0)
}

module.exports = DefineEditText
