let SWFBuffer = require('../swf-buffer.js')

function ExportAssets(tag, options = {}) {
  this.type = 56
  this.count = 0
  this.assets = []

  if (options.swfVersion !== undefined && options.swfVersion < 5 ) {
    console.warn('Warning: the minimal SWF version for ExportAssets is SWF 5.')
  }
  if (tag) {
    let buffer = new SWFBuffer(tag.body)
    this.count = buffer.readUInt16()

    let n = -1
    while (++n < this.count) {
      this.assets.push({
        tag: buffer.readUInt16(),
        name: buffer.readString()
      })
    }
  }
}

ExportAssets.decode = function(buf) {
  return new ExportAssets({body: buf})
}

ExportAssets.prototype.encode = function(options = {}) {
  let buffer = new SWFBuffer(Buffer.alloc(this.getLength()))
  buffer.writeUInt16(this.count)
  for (let i = 0; i < this.count; ++i) {
    buffer.writeUInt16(this.assets[i].tag)
    buffer.writeString(this.assets[i].name)
  }
  return buffer
}

ExportAssets.prototype.getLength = function() {
  let len = 2

  for (let i = 0; i < this.count; ++i) {
    len += 2 + this.assets[i].name.length + 1
  }
  return len
}

module.exports = ExportAssets
