let SWFBuffer = require('../swf-buffer.js')

function SetBackgroundColor(tag) {
  const SWF = require('../index.js')
  this.type = 9
  this.color = new SWF.Color()

  if (tag) {
    let buffer = new SWFBuffer(tag.body)
    this.color = SWF.Color.readRGB(buffer)
  }
}

SetBackgroundColor.decode = function(buf) {
  return new SetBackgroundColor({body: buf})
}

SetBackgroundColor.prototype.encode = function(options = {}) {
  let buffer = new SWFBuffer(Buffer.alloc(3))

  this.color.writeRGB(buffer)
  return buffer
}

SetBackgroundColor.prototype.getLength = function() {
  return 3
}

module.exports = SetBackgroundColor
