let SWFBuffer = require('../swf-buffer.js')

// TODO: issue warning when using invalid tags
function DefineSprite(tag, options = {}) {
  let SWF = require('../index.js')

  this.type = 39
  this.spriteId = 0
  this.frameCount = 0
  this.tags = [
    new SWF.Tag.End()
  ]

  if (options.swfVersion !== undefined && options.swfVersion < 3 ) {
    console.warn('Warning: the minimal SWF version for DefineSprite is SWF 3.')
  }
  if (tag) {
    let buffer = new SWFBuffer(tag.body)
    this.spriteId = buffer.readUInt16()
    this.frameCount = buffer.readUInt16()
    let swf = new SWF()
    swf.buffer = buffer
    this.tags = swf.readTags()
  }
}

DefineSprite.decode = function(buf) {
  return new DefineSprite({body: buf})
}

DefineSprite.prototype.encode = function(options = {}) {
  let SWF = require('../index.js')
  let buffer = new SWFBuffer(Buffer.alloc(this.getLength(options)))
  buffer.writeUInt16(this.spriteId)
  buffer.writeUInt16(this.frameCount)

  let swf = new SWF()
  swf.buffer = buffer
  // TODO: add warning if last tag ain't an End tag
  swf.writeTags(this.tags, options)
  return buffer
}

DefineSprite.prototype.getLength = function(options = {}) {
  let length = 4

  for (let i in this.tags) {
    let tag = this.tags[i]
    length += tag.getLength() + ((tag.getLength() >= 0x3F || (options.keepAnomalies == true && tag.__oddLength == true)) ? 6 : 2)
  }
  return length
}

module.exports = DefineSprite
