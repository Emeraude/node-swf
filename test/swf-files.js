const fs = require('fs')
const test = require('ava')
const SWF = require('../index.js')

const FILEPATH = './test/swf/appendix_a.swf'

console.warn = function() {}

test.cb('SWF.readFile', t => {
  SWF.readFile(FILEPATH, function(err, swf) {
    t.is(err, null)
    t.deepEqual(swf.header.signature, [0x46, 0x57, 0x53])
    t.is(swf.header.version, 3)
    t.is(swf.header.fileLength, 79)
    t.is(swf.header.frameSize.Xmin, 0)
    t.is(swf.header.frameSize.Xmax, 11000)
    t.is(swf.header.frameSize.Ymin, 0)
    t.is(swf.header.frameSize.Ymax, 8000)
    t.is(swf.header.frameRate, 12)
    t.is(swf.header.frameCount, 1)
    t.end()
  })
})

test.cb('SWF.readFile (invalid)', t => {
  SWF.readFile('/non_existing_file.swf', function(err, swf) {
    t.is(err.code, 'ENOENT')
    t.is(swf, undefined)
    t.end()
  })
})
