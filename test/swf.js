const fs = require('fs')
const test = require('ava')
const SWF = require('../index.js')

const FILES = {
  APPENDIX_A: fs.readFileSync('./test/swf/appendix_a.swf'),
  GREEN: fs.readFileSync('./test/swf/green.swf'),
  LOADER: fs.readFileSync('./test/swf/hf-loader.swf')
}

console.warn = function() {}

test('SWF() uncompressed empty', t => {
  let swf = new SWF()
  t.deepEqual(swf.header.signature, [0x46, 0x57, 0x53])
  t.is(swf.header.version, 0)
  t.is(swf.header.fileLength, 13)
  t.is(swf.header.frameSize.Xmin, 0)
  t.is(swf.header.frameSize.Xmax, 0)
  t.is(swf.header.frameSize.Ymin, 0)
  t.is(swf.header.frameSize.Ymax, 0)
  t.is(swf.header.frameRate, 0)
  t.is(swf.header.frameCount, 0)
  t.deepEqual(swf.encode(), Buffer.from([0x46, 0x57, 0x53, 0x00, 0x0D, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]))
})

test('Real file parsing', t => {
  let swf = new SWF(FILES.LOADER)
  t.deepEqual(swf.header.signature, [0x43, 0x57, 0x53])
  t.is(swf.header.version, 8)
  t.is(swf.header.fileLength, 188397)
  t.is(swf.header.frameSize.Xmin, 0)
  t.is(swf.header.frameSize.Xmax, 8400)
  t.is(swf.header.frameSize.Ymin, 0)
  t.is(swf.header.frameSize.Ymax, 10400)
  t.is(swf.header.frameRate, 40)
  t.is(swf.header.frameCount, 5)
  t.is(swf.tags.length, 121)
})

test('Full chain small uncompressed file', t => {
  let swf = new SWF(FILES.APPENDIX_A)
  t.deepEqual(swf.encode({keepAnomalies: true}), FILES.APPENDIX_A)
})

test('Full chain compressed file', t => {
  let swf = new SWF(FILES.GREEN)
  t.deepEqual(swf.encode({keepAnomalies: true}), FILES.GREEN)
})
