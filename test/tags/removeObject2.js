const test = require('ava')
const SWF = require('../../index.js')

test('RemoveObject2()', t => {
  let tag = new SWF.Tag.RemoveObject2()
  t.is(tag.depth, 0)
  t.is(tag.type, 28)
  t.is(tag.getLength(), 2)
})

test('RemoveObject2.decode', t => {
  let tag = SWF.Tag.RemoveObject2.decode(Buffer.from([0x0A, 0x00]))
  t.is(tag.depth, 10)
  t.is(tag.type, 28)
  t.is(tag.getLength(), 2)
})

test('RemoveObject2.encode', t => {
  let tag = new SWF.Tag.RemoveObject2()
  tag.depth = 10
  let buffer = tag.encode()
  t.deepEqual(buffer.buffer, Buffer.from([0x0A, 0x00]))
})
