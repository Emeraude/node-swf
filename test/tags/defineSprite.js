const test = require('ava')
const SWF = require('../../index.js')

// TODO: add full-chain tests using complex sprites (that should be stored in an external file)

test('DefineSprite()', t => {
  let tag = new SWF.Tag.DefineSprite()
  t.is(tag.spriteId, 0)
  t.is(tag.frameCount, 0)
  t.is(tag.tags[0].type, 0)
  t.is(tag.type, 39)
  t.is(tag.getLength(), 6)
})

// TODO: add tests decoding non-empty sprite, as well as sprites containing tags with oddly encoded lengths or non-ending with an End tag
test('DefineSprite.decode empty', t => {
  let tag = SWF.Tag.DefineSprite.decode(Buffer.from([0x0C, 0x00, 0x01, 0x00, 0x00, 0x00]))
  t.is(tag.spriteId, 12)
  t.is(tag.frameCount, 1)
  t.is(tag.tags[0].type, 0)
  t.is(tag.type, 39)
  t.is(tag.getLength(), 6)
})

// TODO: add tests decoding non-empty sprite, as well as sprites containing tags with oddly encoded lengths or non-ending with an End tag
test('DefineSprite.encode empty', t => {
  let tag = new SWF.Tag.DefineSprite()
  tag.spriteId = 12
  tag.frameCount = 1
  let buffer = tag.encode()
  t.deepEqual(buffer.buffer, Buffer.from([0x0C, 0x00, 0x01, 0x00, 0x00, 0x00]))
})
