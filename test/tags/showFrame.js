const test = require('ava')
const SWF = require('../../index.js')

test('ShowFrame()', t => {
  let tag = new SWF.Tag.ShowFrame()
  t.is(tag.type, 1)
  t.is(tag.getLength(), 0)
})

test('ShowFrame.decode', t => {
  let tag = SWF.Tag.ShowFrame.decode()
  t.is(tag.type, 1)
  t.is(tag.getLength(), 0)
})

test('ShowFrame.encode', t => {
  let tag = new SWF.Tag.ShowFrame()
  let buffer = tag.encode()
  t.deepEqual(buffer.buffer, Buffer.from([]))
})
