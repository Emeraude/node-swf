const test = require('ava')
const SWF = require('../../index.js')

test('ExportAssets()', t => {
  let tag = new SWF.Tag.ExportAssets()
  t.is(tag.count, 0)
  t.is(tag.type, 56)
  t.deepEqual(tag.assets, [])
  t.is(tag.getLength(), 2)
})

test('ExportAssets.decode', t => {
  let tag = SWF.Tag.ExportAssets.decode(Buffer.from([0x01, 0x00, 0x5E, 0x00, 0x5F, 0x5F, 0x50, 0x61, 0x63, 0x6B, 0x61, 0x67, 0x65, 0x73, 0x2E, 0x53, 0x74, 0x64, 0x49, 0x6E, 0x63, 0x00]))
  t.is(tag.count, 1)
  t.is(tag.type, 56)
  t.deepEqual(tag.assets, [{tag: 94, name: "__Packages.StdInc"}])
  t.is(tag.getLength(), 22)
})

test('ExportAssets.encode', t => {
  let tag = new SWF.Tag.ExportAssets()
  tag.count = 1
  tag.assets = [{tag: 94, name: "__Packages.StdInc"}]
  let buffer = tag.encode()
  t.deepEqual(buffer.buffer, Buffer.from([0x01, 0x00, 0x5E, 0x00, 0x5F, 0x5F, 0x50, 0x61, 0x63, 0x6B, 0x61, 0x67, 0x65, 0x73, 0x2E, 0x53, 0x74, 0x64, 0x49, 0x6E, 0x63, 0x00]))
})
