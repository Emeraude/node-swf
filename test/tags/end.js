const test = require('ava')
const SWF = require('../../index.js')

test('End()', t => {
  let tag = new SWF.Tag.End()
  t.is(tag.type, 0)
  t.is(tag.getLength(), 0)
})

test('End.decode', t => {
  let tag = SWF.Tag.End.decode()
  t.is(tag.type, 0)
  t.is(tag.getLength(), 0)
})

test('End.encode', t => {
  let tag = new SWF.Tag.End()
  let buffer = tag.encode()
  t.deepEqual(buffer.buffer, Buffer.from([]))
})
