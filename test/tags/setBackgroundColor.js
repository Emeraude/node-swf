const test = require('ava')
const SWF = require('../../index.js')

test('SetBackgroundColor()', t => {
  let tag = new SWF.Tag.SetBackgroundColor()
  t.deepEqual(tag.color, new SWF.Color(0, 0, 0))
  t.is(tag.type, 9)
  t.is(tag.getLength(), 3)
})

test('SetBackgroundColor.decode', t => {
  let tag = SWF.Tag.SetBackgroundColor.decode(Buffer.from([0xCB, 0xCB, 0xDA]))
  t.deepEqual(tag.color, new SWF.Color(203, 203, 218))
  t.is(tag.type, 9)
  t.is(tag.getLength(), 3)
})

test('SetBackgroundColor.encode', t => {
  let tag = new SWF.Tag.SetBackgroundColor()
  tag.color = new SWF.Color(203, 203, 218)
  let buffer = tag.encode()
  t.deepEqual(buffer.buffer, Buffer.from([0xCB, 0xCB, 0xDA]))
})
