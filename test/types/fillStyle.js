const test = require('ava')
const SWF = require('../../index.js')
const SWFBuffer = require('../../swf-buffer.js')

console.warn = function() {}

test('FillStyle()', t => {
  let fs = new SWF.FillStyle()
  t.is(fs.type, 0x00)

  fs = new SWF.FillStyle(0x12)
  t.is(fs.type, 0x12)

  t.throws(() => new SWF.FillStyle(0x11))
})

test('FillStyle.read - type 0x00', t => {
  let buf = new SWFBuffer(Buffer.from([0x00, 0xCB, 0xCD, 0xDA, 0xFF]))
  let fs = SWF.FillStyle.read(buf, {shapeVersion: 1})
  t.is(fs.type, 0x00)
  t.deepEqual(fs.color, new SWF.Color(203, 205, 218))
  t.is(fs.getLength({shapeVersion: 1}), 4)

  buf = new SWFBuffer(Buffer.from([0x00, 0xCB, 0xCD, 0xDA, 0xFF]))
  fs = SWF.FillStyle.read(buf, {shapeVersion: 2})
  t.is(fs.type, 0x00)
  t.deepEqual(fs.color, new SWF.Color(203, 205, 218))
  t.is(fs.getLength({shapeVersion: 2}), 4)

  buf = new SWFBuffer(Buffer.from([0x00, 0xCB, 0xCD, 0xDA, 0xFF]))
  fs = SWF.FillStyle.read(buf, {shapeVersion: 3})
  t.is(fs.type, 0x00)
  t.deepEqual(fs.color, new SWF.Color(203, 205, 218, 255))
  t.is(fs.getLength({shapeVersion: 3}), 5)

  buf = new SWFBuffer(Buffer.from([0x00, 0xCB, 0xCD, 0xDA, 0xFF]))
  fs = SWF.FillStyle.read(buf, {shapeVersion: 4})
  t.is(fs.type, 0x00)
  t.deepEqual(fs.color, new SWF.Color(203, 205, 218, 255))
  t.is(fs.getLength({shapeVersion: 4}), 5)

  buf = new SWFBuffer(Buffer.from([0x00, 0xCB, 0xCD, 0xDA, 0xFF]))
  t.throws(() => SWF.FillStyle.read(buf, {shapeVersion: 5}))
})

test('FillStyle.read - type 0x1X', t => {
  let buf = new SWFBuffer(Buffer.from([0x10, 0x00, 0x00]))
  let fs = SWF.FillStyle.read(buf, {shapeVersion: 1})
  t.is(fs.type, 0x10)
  t.true(fs.gradientMatrix instanceof SWF.Matrix)
  t.true(fs.gradient instanceof SWF.Gradient)
  t.is(fs.getLength({shapeVersion: 1}), 3)

  buf = new SWFBuffer(Buffer.from([0x12, 0x00, 0x00]))
  fs = SWF.FillStyle.read(buf, {shapeVersion: 1})
  t.is(fs.type, 0x12)
  t.true(fs.gradientMatrix instanceof SWF.Matrix)
  t.true(fs.gradient instanceof SWF.Gradient)
  t.is(fs.getLength({shapeVersion: 1}), 3)

  buf = new SWFBuffer(Buffer.from([0x13, 0x00, 0x00, 0x00, 0x00]))
  fs = SWF.FillStyle.read(buf, {shapeVersion: 4})
  t.is(fs.type, 0x13)
  t.true(fs.gradientMatrix instanceof SWF.Matrix)
  t.true(fs.gradient instanceof SWF.FocalGradient)
  t.is(fs.getLength({shapeVersion: 4}), 5)
})

test('FillStyle.read - type 0x4X', t => {
  let buf = new SWFBuffer(Buffer.from([0x40, 0x01, 0x00, 0x00]))
  let fs = SWF.FillStyle.read(buf, {shapeVersion: 1})
  t.is(fs.type, 0x40)
  t.is(fs.bitmapId, 1)
  t.true(fs.bitmapMatrix instanceof SWF.Matrix)
  t.is(fs.getLength({shapeVersion: 1}), 4)

  buf = new SWFBuffer(Buffer.from([0x41, 0x01, 0x00, 0x00]))
  fs = SWF.FillStyle.read(buf, {shapeVersion: 1})
  t.is(fs.type, 0x41)
  t.is(fs.bitmapId, 1)
  t.true(fs.bitmapMatrix instanceof SWF.Matrix)
  t.is(fs.getLength({shapeVersion: 1}), 4)

  buf = new SWFBuffer(Buffer.from([0x42, 0x01, 0x00, 0x00]))
  fs = SWF.FillStyle.read(buf, {shapeVersion: 1})
  t.is(fs.type, 0x42)
  t.is(fs.bitmapId, 1)
  t.true(fs.bitmapMatrix instanceof SWF.Matrix)
  t.is(fs.getLength({shapeVersion: 1}), 4)

  buf = new SWFBuffer(Buffer.from([0x43, 0x01, 0x00, 0x00]))
  fs = SWF.FillStyle.read(buf, {shapeVersion: 1})
  t.is(fs.type, 0x43)
  t.is(fs.bitmapId, 1)
  t.true(fs.bitmapMatrix instanceof SWF.Matrix)
  t.is(fs.getLength({shapeVersion: 1}), 4)
})

test('FillStyle.read - invalid type', t => {
  let buf = new SWFBuffer(Buffer.from([0x11]))
  t.throws(() => SWF.FillStyle.read(buf))
})

test('FillStyle.prototype.write - type 0x00', t => {
  let fs = new SWF.FillStyle(0x00)
  fs.color = new SWF.Color(203, 205, 218, 255)
  t.is(fs.getLength({shapeVersion: 1}), 4)
  t.is(fs.getLength({shapeVersion: 2}), 4)
  t.is(fs.getLength({shapeVersion: 3}), 5)
  t.is(fs.getLength({shapeVersion: 4}), 5)
  t.throws(() => fs.getLength({shapeVersion: 5}, 4))

  let buf = new SWFBuffer(Buffer.alloc(4))
  fs.write(buf, {shapeVersion: 1})
  t.deepEqual(buf.buffer, Buffer.from([0x00, 0xCB, 0xCD, 0xDA]))

  buf = new SWFBuffer(Buffer.alloc(4))
  fs.write(buf, {shapeVersion: 2})
  t.deepEqual(buf.buffer, Buffer.from([0x00, 0xCB, 0xCD, 0xDA]))

  buf = new SWFBuffer(Buffer.alloc(5))
  fs.write(buf, {shapeVersion: 3})
  t.deepEqual(buf.buffer, Buffer.from([0x00, 0xCB, 0xCD, 0xDA, 0xFF]))

  buf = new SWFBuffer(Buffer.alloc(5))
  fs.write(buf, {shapeVersion: 4})
  t.deepEqual(buf.buffer, Buffer.from([0x00, 0xCB, 0xCD, 0xDA, 0xFF]))

  buf = new SWFBuffer(Buffer.alloc(5))
  t.throws(() => fs.write(buf, {shapeVersion: 5}))
})

test('FillStyle.prototype.write - type 0x1X', t => {
  let fs = new SWF.FillStyle(0x10)
  fs.gradientMatrix = new SWF.Matrix()
  fs.gradient = new SWF.Gradient(2, 2)

  let buf = new SWFBuffer(Buffer.alloc(3))
  fs.write(buf, {shapeVersion: 4})
  t.deepEqual(buf.buffer, Buffer.from([0x10, 0x00, 0xA0]))

  buf = new SWFBuffer(Buffer.alloc(3))
  fs.type = 0x12
  fs.write(buf, {shapeVersion: 4})
  t.deepEqual(buf.buffer, Buffer.from([0x12, 0x00, 0xA0]))

  buf = new SWFBuffer(Buffer.alloc(5))
  fs.type = 0x13
  fs.gradient = new SWF.FocalGradient(2, 2, 0.5)
  fs.write(buf, {shapeVersion: 4})
  t.deepEqual(buf.buffer, Buffer.from([0x13, 0x00, 0xA0, 0x80, 0x00]))
})

test('FillStyle.prototype.write - type 0x4X', t => {
  let fs = new SWF.FillStyle(0x40)
  fs.bitmapId = 1
  fs.bitmapMatrix = new SWF.Matrix()

  let buf = new SWFBuffer(Buffer.alloc(3))
  fs.write(buf)
  t.deepEqual(buf.buffer, Buffer.from([0x40, 0x01, 0x00]))

  buf = new SWFBuffer(Buffer.alloc(3))
  fs.type = 0x41
  fs.write(buf)
  t.deepEqual(buf.buffer, Buffer.from([0x41, 0x01, 0x00]))

  buf = new SWFBuffer(Buffer.alloc(3))
  fs.type = 0x42
  fs.write(buf)
  t.deepEqual(buf.buffer, Buffer.from([0x42, 0x01, 0x00]))

  buf = new SWFBuffer(Buffer.alloc(3))
  fs.type = 0x43
  fs.write(buf)
  t.deepEqual(buf.buffer, Buffer.from([0x43, 0x01, 0x00]))
})

test('FillStyle.write - invalid type', t => {
  let fs = new SWF.FillStyle()
  let buf = new SWFBuffer(Buffer.alloc(3))

  fs.type = 0x11
  t.throws(() => fs.getLength())
  t.throws(() => fs.write(buf))
})
