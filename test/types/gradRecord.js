const test = require('ava')
const SWF = require('../../index.js')
const SWFBuffer = require('../../swf-buffer.js')

test('GradRecord()', t => {
  let gr = new SWF.GradRecord()
  t.is(gr.ratio, 0)
  t.deepEqual(gr.color, new SWF.Color())

  gr = new SWF.GradRecord(10)
  t.is(gr.ratio, 10)
  t.deepEqual(gr.color, new SWF.Color())

  gr = new SWF.GradRecord(10, new SWF.Color(255, 255, 255))
  t.is(gr.ratio, 10)
  t.deepEqual(gr.color, new SWF.Color(255, 255, 255))
})

test('GradRecord.read', t => {
  let buf = new SWFBuffer(Buffer.from([0x42, 0xCB, 0xCD, 0xDA, 0xFF]))
  let gr = SWF.GradRecord.read(buf, {shapeVersion: 1})
  t.is(gr.ratio, 66)
  t.deepEqual(gr.color, new SWF.Color(203, 205, 218))

  buf = new SWFBuffer(Buffer.from([0x42, 0xCB, 0xCD, 0xDA, 0xFF]))
  gr = SWF.GradRecord.read(buf, {shapeVersion: 2})
  t.is(gr.ratio, 66)
  t.deepEqual(gr.color, new SWF.Color(203, 205, 218))

  buf = new SWFBuffer(Buffer.from([0x42, 0xCB, 0xCD, 0xDA, 0xFF]))
  gr = SWF.GradRecord.read(buf, {shapeVersion: 3})
  t.is(gr.ratio, 66)
  t.deepEqual(gr.color, new SWF.Color(203, 205, 218, 255))

  buf = new SWFBuffer(Buffer.from([0x42, 0xCB, 0xCD, 0xDA, 0xFF]))
  gr = SWF.GradRecord.read(buf, {shapeVersion: 4})
  t.is(gr.ratio, 66)
  t.deepEqual(gr.color, new SWF.Color(203, 205, 218, 255))

  buf = new SWFBuffer(Buffer.from([0x42, 0xCB, 0xCD, 0xDA, 0xFF]))
  t.throws(() => SWF.GradRecord.read(buf, {shapeVersion: 5}))
  t.throws(() => SWF.GradRecord.read(buf))
})

test('GradRecord.prototype.write', t => {
  let gr = new SWF.GradRecord(66, new SWF.Color(203, 205, 218, 255))

  let buf = new SWFBuffer(Buffer.alloc(4))
  gr.write(buf, {shapeVersion: 1})
  t.deepEqual(buf.buffer, Buffer.from([0x42, 0xCB, 0xCD, 0xDA]))
  t.is(buf.pos, 4)
  t.is(buf.bitPos, 1)

  buf = new SWFBuffer(Buffer.alloc(4))
  gr.write(buf, {shapeVersion: 2})
  t.deepEqual(buf.buffer, Buffer.from([0x42, 0xCB, 0xCD, 0xDA]))
  t.is(buf.pos, 4)
  t.is(buf.bitPos, 1)

  buf = new SWFBuffer(Buffer.alloc(5))
  gr.write(buf, {shapeVersion: 3})
  t.deepEqual(buf.buffer, Buffer.from([0x42, 0xCB, 0xCD, 0xDA, 0xFF]))
  t.is(buf.pos, 5)
  t.is(buf.bitPos, 1)


  buf = new SWFBuffer(Buffer.alloc(5))
  gr.write(buf, {shapeVersion: 4})
  t.deepEqual(buf.buffer, Buffer.from([0x42, 0xCB, 0xCD, 0xDA, 0xFF]))
  t.is(buf.pos, 5)
  t.is(buf.bitPos, 1)

  buf = new SWFBuffer(Buffer.alloc(5))
  t.throws(() => gr.write(buf))
  t.throws(() => gr.write(buf, {shapeVersion: 5}))
})

test('GradRecord.prototype.getLength', t => {
  let gr = new SWF.GradRecord()
  t.is(gr.getLength({shapeVersion: 1}), 4)
  t.is(gr.getLength({shapeVersion: 2}), 4)
  t.is(gr.getLength({shapeVersion: 3}), 5)
  t.is(gr.getLength({shapeVersion: 4}), 5)
  t.throws(() => gr.getLength())
  t.throws(() => gr.getLength({shapeVersion: 5}))
})
