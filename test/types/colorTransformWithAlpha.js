const test = require('ava')
const SWF = require('../../index.js')
const SWFBuffer = require('../../swf-buffer.js')

test('ColorTransformWithAlpha()', t => {
  let rect = new SWF.ColorTransformWithAlpha()
  t.is(rect.getLength(), 1)
})

test('ColorTransformWithAlpha.read', t => {
  let buf = new SWFBuffer(Buffer.from([0xE8, 0x25, 0x09, 0x42, 0x54, 0x00, 0xDA, 0x36, 0x8D, 0xA0, 0x00]))
  let cxform = SWF.ColorTransformWithAlpha.read(buf)
  t.is(cxform.redMult, 37)
  t.is(cxform.greenMult, 37)
  t.is(cxform.blueMult, 37)
  t.is(cxform.alphaMult, 256)
  t.is(cxform.redAdd, 218)
  t.is(cxform.greenAdd, 218)
  t.is(cxform.blueAdd, 218)
  t.is(cxform.alphaAdd, 0)
  t.is(cxform.getLength(), 11)
  t.is(buf.pos, 11)
  t.is(buf.bitPos, 1)

  buf = new SWFBuffer(Buffer.from([0x69, 0x00, 0x40, 0x10, 0x01, 0x70]))
  cxform = SWF.ColorTransformWithAlpha.read(buf)
  t.is(cxform.redMult, 256)
  t.is(cxform.greenMult, 256)
  t.is(cxform.blueMult, 256)
  t.is(cxform.alphaMult, 92)
  t.is(cxform.getLength(), 6)
  t.is(buf.pos, 6)
  t.is(buf.bitPos, 1)

  buf = new SWFBuffer(Buffer.from([0xA9, 0x00, 0x40, 0x10, 0x01, 0x70]))
  cxform = SWF.ColorTransformWithAlpha.read(buf)
  t.is(cxform.redAdd, 256)
  t.is(cxform.greenAdd, 256)
  t.is(cxform.blueAdd, 256)
  t.is(cxform.alphaAdd, 92)
  t.is(cxform.getLength(), 6)
  t.is(buf.pos, 6)
  t.is(buf.bitPos, 1)

  buf = new SWFBuffer(Buffer.from([0x00]))
  cxform = SWF.ColorTransformWithAlpha.read(buf)
  t.is(cxform.getLength(), 1)
  t.is(buf.pos, 1)
  t.is(buf.bitPos, 1)
})

test('ColorTransformWithAlpha.prototype.write', t => {
  let buf = new SWFBuffer(Buffer.alloc(11))
  let cxform = new SWF.ColorTransformWithAlpha()
  cxform.redMult = 37
  cxform.greenMult = 37
  cxform.blueMult = 37
  cxform.alphaMult = 256
  cxform.redAdd = 218
  cxform.greenAdd = 218
  cxform.blueAdd = 218
  cxform.alphaAdd = 0
  t.is(cxform.getLength(), 11)
  cxform.write(buf)
  t.deepEqual(buf.buffer, Buffer.from([0xE8, 0x25, 0x09, 0x42, 0x54, 0x00, 0xDA, 0x36, 0x8D, 0xA0, 0x00]))
  t.is(buf.pos, 11)
  t.is(buf.bitPos, 1)

  buf = new SWFBuffer(Buffer.alloc(6))
  cxform = new SWF.ColorTransformWithAlpha()
  cxform.redMult = 256
  cxform.greenMult = 256
  cxform.blueMult = 256
  cxform.alphaMult = 92
  t.is(cxform.getLength(), 6)
  cxform.write(buf)
  t.deepEqual(buf.buffer, Buffer.from([0x69, 0x00, 0x40, 0x10, 0x01, 0x70]))
  t.is(buf.pos, 6)
  t.is(buf.bitPos, 1)

  buf = new SWFBuffer(Buffer.alloc(6))
  cxform = new SWF.ColorTransformWithAlpha()
  cxform.redAdd = 256
  cxform.greenAdd = 256
  cxform.blueAdd = 256
  cxform.alphaAdd = 92
  t.is(cxform.getLength(), 6)
  cxform.write(buf)
  t.deepEqual(buf.buffer, Buffer.from([0xA9, 0x00, 0x40, 0x10, 0x01, 0x70]))
  t.is(buf.pos, 6)
  t.is(buf.bitPos, 1)

  buf = new SWFBuffer(Buffer.alloc(1))
  cxform = new SWF.ColorTransformWithAlpha()
  t.is(cxform.getLength(), 1)
  cxform.write(buf)
  t.deepEqual(buf.buffer, Buffer.from([0x00]))
  t.is(buf.pos, 1)
  t.is(buf.bitPos, 1)
})
