const test = require('ava')
const SWF = require('../../index.js')
const SWFBuffer = require('../../swf-buffer.js')

console.warn = function() {}

test('FillStyleArray()', t => {
  let fsa = new SWF.FillStyleArray()
  t.true(fsa instanceof Array)
})

test('FillStyleArray.prototype.push', t => {
  let fsa = new SWF.FillStyleArray()

  t.throws(() => fsa.push('foobar'))
  t.throws(() => fsa.push(42))
  t.throws(() => fsa.push([]))
  t.throws(() => fsa.push({}))
  t.throws(() => fsa.push(true))
  t.throws(() => fsa.push(new SWF.Rect()))
  fsa.push(new SWF.FillStyle())
  t.deepEqual(fsa[0], new SWF.FillStyle())
})

test('FillStyleArray.read', t => {
  let buf = new SWFBuffer(Buffer.from([0x01, 0x00, 0x00, 0x00, 0x00]))
  let fsa = SWF.FillStyleArray.read(buf, {shapeVersion: 1})
  t.is(fsa.length, 1)
  t.is(fsa.getLength({shapeVersion: 1}), 5)
  t.true(fsa[0] instanceof SWF.FillStyle)

  buf = new SWFBuffer(Buffer.concat([Buffer.from([0xFF, 0x00, 0x01]) ,Buffer.alloc(2000)]))
  fsa = SWF.FillStyleArray.read(buf, {shapeVersion: 2})
  t.is(fsa.length, 256)
  t.is(fsa.getLength({shapeVersion: 2}), 1027)
  t.true(fsa[255] instanceof SWF.FillStyle)

  buf = new SWFBuffer(Buffer.concat([Buffer.from([0xFF, 0x00, 0x01]) ,Buffer.alloc(2000)]))
  fsa = SWF.FillStyleArray.read(buf, {shapeVersion: 1})
  t.is(fsa.length, 255)
  t.is(fsa.getLength({shapeVersion: 1}), 1021)
  t.true(fsa[254] instanceof SWF.FillStyle)

  buf = new SWFBuffer(Buffer.from([0x01, 0x00, 0x00, 0x00, 0x00]))
  t.throws(() => fsa = SWF.FillStyleArray.read(buf))
})

test('FillStyleArray.prototype.write', t => {
  let fsa = new SWF.FillStyleArray()
  let buf = new SWFBuffer(Buffer.alloc(5))
  fsa.push(new SWF.FillStyle())
  fsa[0].color = new SWF.Color(203, 205, 218, 255)
  t.is(fsa.length, 1)
  t.is(fsa.getLength({shapeVersion: 1}), 5)
  fsa.write(buf, {shapeVersion: 1})
  t.deepEqual(buf.buffer, Buffer.from([0x01, 0x00, 0xCB, 0xCD, 0xDA]))

  buf = new SWFBuffer(Buffer.alloc(5))
  t.throws(() => fsa.getLength())
  t.throws(() => fsa.write(buffer))

  fsa = new SWF.FillStyleArray()
  buf = new SWFBuffer(Buffer.alloc(2000))
  for (let i = 0; i < 256; ++i) {
    fsa.push(new SWF.FillStyle())
    fsa[i].color = new SWF.Color(203, 205, 218, 255)
  }
  t.is(fsa.length, 256)
  t.is(fsa.getLength({shapeVersion: 2}), 1027)
  fsa.write(buf, {shapeVersion: 2})
  t.is(buf.buffer[0], 0xFF)
  t.is(buf.buffer[1], 0x00)
  t.is(buf.buffer[2], 0x01)

  fsa = new SWF.FillStyleArray()
  buf = new SWFBuffer(Buffer.alloc(2000))
  for (let i = 0; i < 255; ++i) {
    fsa.push(new SWF.FillStyle())
    fsa[i].color = new SWF.Color(203, 205, 218, 255)
  }
  t.is(fsa.length, 255)
  t.is(fsa.getLength({shapeVersion: 1}), 1021)
  fsa.write(buf, {shapeVersion: 1})
  t.is(buf.buffer[0], 0xFF)
})
