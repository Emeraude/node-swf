const test = require('ava')
const SWF = require('../../index.js')
const SWFBuffer = require('../../swf-buffer.js')

test('Rect()', t => {
  let rect = new SWF.Rect()
  t.is(rect.Xmin, 0)
  t.is(rect.Xmax, 0)
  t.is(rect.Ymin, 0)
  t.is(rect.Ymax, 0)
  t.is(rect.getLength(), 1)
})

test('Rect.read', t => {
  let buf = new SWFBuffer(Buffer.from([0x78, 0x00, 0x04, 0x1A, 0x00, 0x00, 0x14, 0x50, 0x00]))
  let rect = SWF.Rect.read(buf)
  t.is(rect.Xmin, 0)
  t.is(rect.Xmax, 8400)
  t.is(rect.Ymin, 0)
  t.is(rect.Ymax, 10400)
  t.is(rect.getLength(), 9)
  t.is(buf.pos, 9)
  t.is(buf.bitPos, 1)

  buf = new SWFBuffer(Buffer.from([0x77, 0xFB, 0x0D, 0xF9, 0xFF, 0xB0, 0x0D, 0x68]))
  rect = SWF.Rect.read(buf)
  t.is(rect.Xmin, -40)
  t.is(rect.Xmax, 7155)
  t.is(rect.Ymin, -40)
  t.is(rect.Ymax, 429)
  t.is(rect.getLength(), 8)
  t.is(buf.pos, 8)
  t.is(buf.bitPos, 1)
})

test('Rect.prototype.write', t => {
  let buf = new SWFBuffer(Buffer.alloc(9))
  let rect = new SWF.Rect()
  rect.Xmin = 0
  rect.Xmax = 8400
  rect.Ymin = 0
  rect.Ymax = 10400
  t.is(rect.getLength(), 9)
  rect.write(buf)
  t.deepEqual(buf.buffer, Buffer.from([0x78, 0x00, 0x04, 0x1A, 0x00, 0x00, 0x14, 0x50, 0x00]))
  t.is(buf.pos, 9)
  t.is(buf.bitPos, 1)

  buf = new SWFBuffer(Buffer.alloc(8))
  rect = new SWF.Rect()
  rect.Xmin = -40
  rect.Xmax = 7155
  rect.Ymin = -40
  rect.Ymax = 429
  t.is(rect.getLength(), 8)
  rect.write(buf)
  t.deepEqual(buf.buffer, Buffer.from([0x77, 0xFB, 0x0D, 0xF9, 0xFF, 0xB0, 0x0D, 0x68]))
  t.is(buf.pos, 8)
  t.is(buf.bitPos, 1)
})
