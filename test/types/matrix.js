const test = require('ava')
const SWF = require('../../index.js')
const SWFBuffer = require('../../swf-buffer.js')

console.warn = function() {}

test('Matrix()', t => {
  let matrix = new SWF.Matrix()
  t.is(matrix.translateX, 0)
  t.is(matrix.translateY, 0)
})

test('Matrix.read', t => {
  let buf = new SWFBuffer(Buffer.from([0x84, 0xC9, 0x00, 0x00, 0xC0, 0x00, 0x1C, 0xFA, 0x03, 0x0C, 0x60]))
  let matrix = SWF.Matrix.read(buf)
  t.is(matrix.__oddNScaleBits, 1)
  t.is(matrix.scaleX, 0)
  t.is(matrix.scaleY, 0)
  t.is(matrix.rotateSkew0, 65536)
  t.is(matrix.rotateSkew1, -65536)
  t.is(matrix.translateX, 8000)
  t.is(matrix.translateY, 6243)
  t.is(matrix.getLength({keepAnomalies: true}), 11)
  t.is(matrix.getLength({keepAnomalies: false}), 11)
  t.is(buf.pos, 11)
  t.is(buf.bitPos, 1)

  buf = new SWFBuffer(Buffer.from([0x83, 0x24, 0x00, 0x03, 0x00, 0x00, 0x73, 0xE8, 0x0C, 0x31, 0x80]))
  matrix = SWF.Matrix.read(buf)
  t.is(matrix.scaleX, 0)
  t.is(matrix.scaleY, 0)
  t.is(matrix.rotateSkew0, 65536)
  t.is(matrix.rotateSkew1, -65536)
  t.is(matrix.translateX, 8000)
  t.is(matrix.translateY, 6243)
  t.is(matrix.getLength(), 11)
  t.is(buf.pos, 11)
  t.is(buf.bitPos, 1)

  buf = new SWFBuffer(Buffer.from([0x00]))
  matrix = SWF.Matrix.read(buf)
  t.is(matrix.translateX, 0)
  t.is(matrix.translateY, 0)
  t.is(matrix.getLength(), 1)
  t.is(buf.pos, 1)
  t.is(buf.bitPos, 1)
})

test('Matrix.prototype.write', t => {
  let buf = new SWFBuffer(Buffer.alloc(11))
  let matrix = new SWF.Matrix()
  matrix.scaleX = 0
  matrix.scaleY = 0
  matrix.rotateSkew0 = 65536
  matrix.rotateSkew1 = -65536
  matrix.translateX = 8000
  matrix.translateY = 6243
  t.is(matrix.getLength(), 11)
  matrix.write(buf)
  t.deepEqual(buf.buffer, Buffer.from([0x83, 0x24, 0x00, 0x03, 0x00, 0x00, 0x73, 0xE8, 0x0C, 0x31, 0x80]))
  t.is(buf.pos, 11)
  t.is(buf.bitPos, 1)

  buf = new SWFBuffer(Buffer.alloc(11))
  matrix = new SWF.Matrix()
  matrix.__oddNScaleBits = 1
  matrix.scaleX = 0
  matrix.scaleY = 0
  matrix.rotateSkew0 = 65536
  matrix.rotateSkew1 = -65536
  matrix.translateX = 8000
  matrix.translateY = 6243
  t.is(matrix.getLength({keepAnomalies: true}), 11)
  t.is(matrix.getLength({keepAnomalies: false}), 11)
  matrix.write(buf, {keepAnomalies: true})
  t.deepEqual(buf.buffer, Buffer.from([0x84, 0xC9, 0x00, 0x00, 0xC0, 0x00, 0x1C, 0xFA, 0x03, 0x0C, 0x60]))
  t.is(buf.pos, 11)
  t.is(buf.bitPos, 1)

  buf = new SWFBuffer(Buffer.alloc(1))
  matrix = new SWF.Matrix()
  matrix.translateX = 0
  matrix.translateY = 0
  t.is(matrix.getLength(), 1)
  matrix.write(buf)
  t.is(buf.pos, 1)
  t.is(buf.bitPos, 1)
  t.deepEqual(buf.buffer, Buffer.from([0x00]))
})
