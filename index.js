let fs = require('fs')
let zlib = require('zlib')
let SWFBuffer = require('./swf-buffer.js')
let tags = require('./swf-tags.js')
let Color = require('./types/color.js')
let ColorTransform = require('./types/colorTransform.js')
let ColorTransformWithAlpha = require('./types/colorTransformWithAlpha.js')
let FillStyle = require('./types/fillStyle.js')
let FillStyleArray = require('./types/fillStyleArray.js')
let FocalGradient = require('./types/focalGradient.js')
let Gradient = require('./types/gradient.js')
let GradRecord = require('./types/gradRecord.js')
let LineStyle = require('./types/lineStyle.js')
let LineStyle2 = require('./types/lineStyle2.js')
let Matrix = require('./types/matrix.js')
let Rect = require('./types/rect.js')

function uncompressBuffer(buffer) {
  switch (buffer[0]) {
  case 0x43: // zlib compressed
    return Buffer.concat([buffer.slice(0, 8),
                          Buffer.from(zlib.inflateSync(buffer.slice(8)))])

  case 0x5A: // LZMA compressed
    throw new Error('LZMA compression not implemented')
    return

  case 0x46: // not compressed
    return buffer

  default:
    throw new Error('Invalid compression method')
  }
}

function compressBuffer(buffer) {
  switch (buffer[0]) {
  case 0x43: // zlib compressed
    return Buffer.concat([buffer.slice(0, 8),
                          Buffer.from(zlib.deflateSync(buffer.slice(8)))])

  case 0x5A: // LZMA compressed
    throw new Error('LZMA compression not implemented')
    return

  case 0x46: // not compressed
    return buffer

  default:
    throw new Error('Invalid compression method')
  }
}

function SWF(buffer) {
  if (buffer) {
    this.buffer = new SWFBuffer(uncompressBuffer(buffer))
    this.header = this.readHeader()
    this.tags = this.readTags()
  } else {
    this.header = {
      signature: [0x46, 0x57, 0x53],
      version: 0,
      fileLength: 13,
      frameSize: new Rect(),
      frameRate: 0,
      frameCount: 0
    }
    this.tags = []
  }
}

SWF.prototype.readHeader = function() {
  let header = {}

  header.signature = [this.buffer.readUInt8(),
                      this.buffer.readUInt8(),
                      this.buffer.readUInt8()]
  header.version = this.buffer.readUInt8()
  header.fileLength = this.buffer.readUInt32()
  header.frameSize = Rect.read(this.buffer)
  header.frameRate = parseInt(this.buffer.readFixed8())
  header.frameCount = this.buffer.readUInt16()

  return header
}

SWF.prototype.readTag = function() {
  let tag = {}
  let length = this.buffer.readUInt16()
  let oddLength = false

  tag.type = length >> 6
  tag.length = length & 0x3F
  if (tag.length == 0x3F) {
    tag.length = this.buffer.readUInt32()
    if (tag.length < 0x3F) {
      oddLength = true
      console.warn('Warning: The length of this tag is oddly encoded')
    }
  }

  tag.body = this.buffer.readBytes(tag.length)
  tag = new (tags.getTagClass(tag.type))(tag)

  if (oddLength === true) {
    tag.__oddLength = true
  }
  return tag
}

SWF.prototype.readTags = function() {
  let tags = []
  let lastTag

  do {
    lastTag = this.readTag()
    tags.push(lastTag)
  } while (lastTag.type != 0)
  return tags
}

SWF.readFile = function(file, cb) {
  fs.readFile(file, function(err, swfBuffer) {
    cb(err, !err ? new SWF(swfBuffer) : undefined)
  })
}

SWF.prototype.writeHeader = function(header, options) {
  this.buffer.writeUInt8(header.signature[0])
  this.buffer.writeUInt8(header.signature[1])
  this.buffer.writeUInt8(header.signature[2])
  this.buffer.writeUInt8(header.version)
  this.buffer.writeUInt32(header.fileLength) // TODO: we should update this
  header.frameSize.write(this.buffer)
  this.buffer.writeFixed8(header.frameRate)
  this.buffer.writeUInt16(header.frameCount)
}

SWF.prototype.writeTag = function(tag, options) {
  let tagCodeAndLength = tag.type << 6
  let length = tag.getLength()

  if (length >= 0x3F || (options.keepAnomalies == true && tag.__oddLength == true)) {
    tagCodeAndLength |= 0x3F
    this.buffer.writeUInt16(tagCodeAndLength)
    this.buffer.writeUInt32(length)
  } else {
    tagCodeAndLength |= length
    this.buffer.writeUInt16(tagCodeAndLength)
  }

  this.buffer.writeBytes(tag.encode(options).buffer)
}

SWF.prototype.writeTags = function(tags, options) {
  for (let i in tags) {
    const tag = tags[i]
    this.writeTag(tag, options)
  }
}

SWF.prototype.writeFile = function(file, options = {}, cb) {
  fs.writeFile(file, this.encode(options), cb)
}

SWF.prototype.encode = function(options = {}) {
  this.buffer = new SWFBuffer(Buffer.alloc(this.header.fileLength));

  // TODO: add warning for ill-formated metadata (especially in the header)
  this.writeHeader(this.header, options)
  this.writeTags(this.tags, options)
  return compressBuffer(this.buffer.buffer)
}

module.exports = SWF
module.exports.Tag = tags.Tag
module.exports.Color = Color
module.exports.ColorTransform = ColorTransform
module.exports.ColorTransformWithAlpha = ColorTransformWithAlpha
module.exports.FillStyle = FillStyle
module.exports.FillStyleArray = FillStyleArray
module.exports.FocalGradient = FocalGradient
module.exports.Gradient = Gradient
module.exports.GradRecord = GradRecord
module.exports.LineStyle = LineStyle
module.exports.LineStyle2 = LineStyle2
module.exports.Matrix = Matrix
module.exports.Rect = Rect
